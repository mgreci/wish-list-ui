import React from 'react';

export const ListContext = React.createContext(null);
export const ListContextDefault = {
  list: [],
};
