export const centsToDollars = cents => (cents / 100).toFixed(2);

// input format: $123.45 or 43.21
export const dollarsToCents = dollars => {
	// remove $ and .
	return Number(dollars.replace(/$\./g, ''));
};

