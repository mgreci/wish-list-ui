import { useState, useContext } from 'react';
import { ListContext } from '../list';
import { Card, Collapse } from '@blueprintjs/core';
import List from './List';

const PurchasedList = () => {
  const { purchasedWishes } = useContext(ListContext);
  const [ show, setShow ] = useState(false);
  const handleClick = () => setShow(!show);
  return (
    <>
      <Card interactive onClick={handleClick}>
        Purchased List
      </Card>
      <Collapse isOpen={show}>
        <List items={purchasedWishes} />
      </Collapse>
    </>
  );
};

export default PurchasedList;