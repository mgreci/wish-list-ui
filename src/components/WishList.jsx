import { useContext, useState } from "react"
import { ListContext } from '../list';
import { Card, Collapse } from '@blueprintjs/core';
import List from './List';

const WishList = () => {
  const { wishes } = useContext(ListContext);
  const [ show, setShow ] = useState(true);
  const list = wishes.filter(w => !w.purchased);

  const toggleCollapse = () => setShow(!show);

  return (
    <>
      <Card interactive onClick={toggleCollapse}>
        Wishes {list.length}
        <Collapse isOpen={show}>
          <List items={list} />
        </Collapse>
      </Card>
    </>
  )
};

export default WishList;