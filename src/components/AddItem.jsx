import { Button, Card, Classes, FormGroup, InputGroup, Intent, Slider } from '@blueprintjs/core';
import JSConfetti from 'js-confetti';
import { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import { AppToaster } from "../toaster";
import { ListContext } from '../list';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const StyledCard = styled(Card)`
  min-width: 50%;
  max-width: 30rem;
`;

const AddItem = () => {
  const { addWish } = useContext(ListContext);
  const [ description, setDescription ] = useState('');
  const [ estimate, setEstimate ] = useState(0);
  const [ priority, setPriority ] = useState(1);
  const [ jsConfetti, setJsConfetti ] = useState({});

  useEffect(() => {
    setJsConfetti(new JSConfetti());
  }, []);

  const handleChange = (property, value) => {
    // console.log({ property, value });
    switch (property) {
      case 'description':
        setDescription(value);
        break;
      case 'estimate':
        setEstimate(Number(value));
        break;
      case 'priority':
        setPriority(value);
        break;
      default:
        console.error('unknown property: ', property);
        break;
    }
  };

  const allowAdd = description && typeof estimate === 'number' && estimate > 0 && priority > 0;

  const handleAddItem = () => {
    addWish({
      description,
      estimate,
      priority,
    })
    .then(() => {
      // clear
      setDescription('');
      setEstimate(0);
      setPriority(1);

      // indicate success
      AppToaster.show({
        message: "Item added",
        intent: Intent.SUCCESS,
      });
      jsConfetti.addConfetti({
        emojis: ['💰', '🤑', '💲'],
      });
    })
    .catch(error => {
      // indicate error
      AppToaster.show({
        message: "Unable to add item",
        intent: Intent.DANGER,
      });
      console.error(error);
    });
  };

  return (
    <Container>
      <StyledCard>
        <FormGroup label="Item description/link">
          <InputGroup value={description} onChange={({ target: { value } }) => handleChange('description', value)} />
        </FormGroup>
        <FormGroup label="Estimate $">
          <InputGroup value={estimate} type="number" onChange={({ target: { value } }) => handleChange('estimate', value)} />
        </FormGroup>
        <FormGroup label="Priority">
          <Slider
            min={1}
            max={5}
            stepSize={1}
            labelStepSize={1}
            onChange={val => handleChange('priority', val)}
            value={priority}
          />
        </FormGroup>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button>Cancel</Button>
          <Button onClick={handleAddItem} intent={Intent.SUCCESS} disabled={!allowAdd}>Add</Button>
        </div>
      </StyledCard>
    </Container>
  );
};

export default AddItem;