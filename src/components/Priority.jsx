import { Icon } from '@blueprintjs/core';

const Priority = ({ priority }) => {
  const dummyArray = [1,2,3,4,5];
  return (
    dummyArray.map(value => <Icon key={value} icon={value <= priority ? "star" : "star-empty"} />)
  );
};

export default Priority;