import { EditableText, Icon, Intent } from '@blueprintjs/core';
import { IconNames } from "@blueprintjs/icons";
import { centsToDollars } from '../utils';
import Priority from './Priority';

const actionsStyles = {
  display: 'flex',
  justifyContent: 'space-evenly',
};

const Wish = ({ wish }) => {
  const handlePurchased = () => {
    console.log('purchase this wish...', wish);
  };

  const handleEdit = () => {
    console.log('edit this wish...', wish);
  };

  const handleDelete = () => {
    console.log('delete this wish...', wish);
  };

  return (
    <tr>
      <td><EditableText defaultValue={wish.description} /></td>
      <td><EditableText defaultValue={centsToDollars(wish.estimate)} /></td>
      <td><Priority priority={wish.priority} /></td>
      <td>
        <div style={actionsStyles}>
          <Icon icon={IconNames.TICK} iconSize={Icon.SIZE_LARGE} intent={Intent.SUCCESS} onClick={handlePurchased}/>
          <Icon icon={IconNames.EDIT} iconSize={Icon.SIZE_LARGE} onClick={handleEdit} />
          <Icon icon={IconNames.DELETE} iconSize={Icon.SIZE_LARGE} intent={Intent.DANGER} onClick={handleDelete} />
        </div>
      </td>
    </tr>
  );
};

export default Wish;