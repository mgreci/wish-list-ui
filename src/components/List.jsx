import { HTMLTable } from '@blueprintjs/core';
import Wish from './Wish';

const tableStyles = {
  width: '100%',
};

const List = ({ items }) => {
  if (!items || items.length < 1) {
    return <h5>No wishes</h5>
  }

  return (
    <HTMLTable bordered striped style={tableStyles}>
      <thead>
        <tr>
          <th>Description</th>
          <th>Estimate $</th>
          <th>Priority</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {items.map(wish => <Wish key={wish.id} wish={wish} />)}
      </tbody>
    </HTMLTable>
  )
};

export default List;