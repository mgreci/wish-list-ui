import { Tab, Tabs } from '@blueprintjs/core';
import { useEffect, useState } from 'react';
import io from 'socket.io-client';
import styled from 'styled-components';
import { addItem } from './api';
import { ListContext, ListContextDefault } from './list';
import AddItem from './components/AddItem';
import List from './components/List';

const { REACT_APP_API_HOST: API_HOST } = process.env; 

const Container = styled.div`
  max-width: 1160px;
  margin: 0 auto;
  padding: 0.5rem;
`;

const App = () => {
  const [ list, setList ] = useState(ListContextDefault.list);
  const [ selectedTab, setSelectedTab ] = useState("wish");
  const wishes = list ? list.filter(w => !w.purchased) : [];
  const purchased = list ? list.filter(w => w.purchased) : [];

  useEffect(() => {
    const socket = io(`ws://${API_HOST}`);
    socket.on('list', data => {
      console.log('socket list: ', data);
      setList(data);
    });

    return () => socket.disconnect();
  }, []);

  const addWish = async (wish) => {
    const newWish = await addItem(wish);
    setList([...list, newWish]);
  };

  const handleTabChange = newTab => setSelectedTab(newTab);

  return (
    <ListContext.Provider value={{ list, addWish }} >
      <Container>
        <Tabs onChange={handleTabChange} selectedTabId={selectedTab} >
          <Tab id="wish" title="Wishes" panel={<List items={wishes} />} />
          <Tab id="purchased" title="Purchased" panel={<List items={purchased} />} />
          <Tab id="add" title="Add" panel={<AddItem />} />
        </Tabs>
      </Container>
    </ListContext.Provider>
  );
};

export default App;