const { REACT_APP_API_HOST: API_HOST } = process.env;

export const getList = async () => {
	const response = await fetch(`http://${API_HOST}/list`);
	if (!response.ok) {
		throw new Error(`getList error: ${response.status} = ${response.statusText}`);
	}
	const result = await response.json();
	// console.log('getList', { data: result.data });
	return result.data;
};

export const addItem = async (item) => {
	const response = await fetch(`http://${API_HOST}/item`, {
		method: 'POST',
		body: JSON.stringify(item),
	});

	if (!response.ok) {
		throw new Error(`addItem error: ${response.status} = ${response.statusText}`);
	}

	const result = await response.json();
	// console.log('addItem', { result });
	return result;
};